import 'dart:io';
import 'package:amplify_analytics_pinpoint/amplify_analytics_pinpoint.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import '../model/base_response.dart';
import '../model/fetch_token_response.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';

class AmplifyService {
  String standardError = "Sorry! Some error occurred.";

  Future<bool> getUserState() async {
    try {
      AuthSession authSession = await Amplify.Auth.fetchAuthSession(
          options: CognitoSessionOptions(getAWSCredentials: true));
      return authSession.isSignedIn;
    } on AuthException catch (e) {
      return false;
    }
  }

  Future<FetchTokenResponse> getToken() async {
    FetchTokenResponse response;
    try {
      CognitoAuthSession cognitoAuthSession =
          await Amplify.Auth.fetchAuthSession(
              options: CognitoSessionOptions(getAWSCredentials: true));
      response = FetchTokenResponse(
          status: true,
          sub: cognitoAuthSession.userSub,
          idToken: cognitoAuthSession.userPoolTokens.idToken,
          identityId: cognitoAuthSession.identityId,
          refreshToken: cognitoAuthSession.userPoolTokens.refreshToken,
          accessToken: cognitoAuthSession.userPoolTokens.accessToken);
    } on AuthException catch (e) {
      response = FetchTokenResponse(status: false);
    }
    return response;
  }

  Future<BaseResponse> signUp(String username, String password,
      Map<String, String> userAttributes) async {
    Map<CognitoUserAttributeKey, String> cognitoMap =
        Map.castFrom(userAttributes);
    BaseResponse response;

    try {
      SignUpResult res = await Amplify.Auth.signUp(
          username: username,
          password: password,
          options: CognitoSignUpOptions(userAttributes: cognitoMap));
      if (res.isSignUpComplete == true) {
        response = BaseResponse(
          status: true,
          tag: "Sign up successful!",
          message: "A verification code has been sent to" +
              res.nextStep.codeDeliveryDetails.destination,
        );
      } else {
        response = BaseResponse(
            status: true,
            tag: "Confirm User",
            message: "A verification code has been sent to" +
                res.nextStep.codeDeliveryDetails.destination);
      }
    } on AuthException catch (e) {
      response = BaseResponse(
          status: false, tag: "Sign up failed", message: e.message);
    }
    return response;
  }

  Future<BaseResponse> confirmSignUp(
      String userName, String confirmationCode) async {
    BaseResponse response;
    try {
      SignUpResult res = await Amplify.Auth.confirmSignUp(
          username: userName, confirmationCode: confirmationCode);
      if (res.isSignUpComplete) {
        response = BaseResponse(
          status: true,
          tag: "Success!",
          message: "User has been confirmed!",
        );
      }
    } on AuthException catch (e) {
      response = BaseResponse(
        status: false,
        tag: "Confirmation failed!",
        message: e.message,
      );
    }
    return response;
  }

  Future<BaseResponse> resendCode(String userName) async {
    BaseResponse response;
    try {
      ResendSignUpCodeResult res =
          await Amplify.Auth.resendSignUpCode(username: userName);
      response = BaseResponse(
          status: true,
          message: "A verification code has been sent to" +
              res.codeDeliveryDetails.destination,
          tag: "Confirmation code sent");
    } on AuthException catch (e) {
      response = BaseResponse(
          status: false,
          message: e.message,
          tag: "Confirmation code resend failed");
    }
    return response;
  }

  Future<BaseResponse> forgotPassword(String userName) async {
    BaseResponse response;
    try {
      ResetPasswordResult res = await Amplify.Auth.resetPassword(
        username: userName,
      );
      response = BaseResponse(
          status: true,
          tag: "Forgot Password",
          message: "A Confirmation code has been sent to" +
              res.nextStep.codeDeliveryDetails.destination);
    } on AuthException catch (e) {
      response = BaseResponse(
          status: false, tag: "Forgot Password", message: e.message);
    }
    return response;
  }

  Future<BaseResponse> confirmForgotPassword(
      String userName, String newPassword, String confirmationCode) async {
    BaseResponse response;
    try {
      await Amplify.Auth.confirmResetPassword(
          username: userName,
          newPassword: newPassword,
          confirmationCode: confirmationCode);
      response = BaseResponse(
          status: true,
          message: "Password successfully changed!",
          tag: "Forgot Password");
    } on AuthException catch (e) {
      response = BaseResponse(
          status: false, tag: "Forgot Password", message: e.message);
    }
    return response;
  }

  Future<FetchTokenResponse> signIn(String userName, String password) async {
    SignInResult res;
    try {
      res = await Amplify.Auth.signIn(
        username: userName,
        password: password,
      );
    } on AuthException catch (e) {
      return FetchTokenResponse(
          status: false, tag: "Log In Error", message: e.message);
    }
    if (res.isSignedIn) {
      try {
        CognitoAuthSession cognitoAuthSession =
            await Amplify.Auth.fetchAuthSession(
                options: CognitoSessionOptions(getAWSCredentials: true));

        return FetchTokenResponse(
            status: true,
            sub: cognitoAuthSession.userSub,
            idToken: cognitoAuthSession.userPoolTokens.idToken,
            identityId: cognitoAuthSession.identityId,
            refreshToken: cognitoAuthSession.userPoolTokens.refreshToken,
            accessToken: cognitoAuthSession.userPoolTokens.accessToken);
      } on AuthException catch (e) {
        return FetchTokenResponse(status: false);
      }
    } else {
      return FetchTokenResponse(
          status: false, tag: "Log In Error", message: standardError);
    }
  }

  Future<BaseResponse> signOut() async {
    BaseResponse response;
    try {
      Amplify.Auth.signOut();
      response = BaseResponse(
          status: true, tag: "Sign Out", message: "Sign Out successful!");
    } on AuthException catch (e) {
      response = BaseResponse(
          status: false, tag: "Sign Out Error", message: e.message);
    }
    return response;
  }

  Future<BaseResponse> resetPassword(
      {String currentPassword, String newPassword}) async {
    BaseResponse response;
    try {
      await Amplify.Auth.updatePassword(
          newPassword: newPassword, oldPassword: currentPassword);
      response = BaseResponse(
          status: true,
          message: "Your password has been reset successfully!",
          tag: "Reset Password");
    } on AuthException catch (e) {
      response = BaseResponse(status: false, message: e.message);
    }
    return response;
  }

  Future<BaseResponse> uploadFile({
    File file,
    String key,
  }) async {
    BaseResponse response;
    try {
      S3UploadFileOptions options =
          S3UploadFileOptions(accessLevel: StorageAccessLevel.private);
      UploadFileResult result = await Amplify.Storage.uploadFile(
          key: key, local: file, options: options);
      response = BaseResponse(
        status: true,
        tag: "Upload Successful",
        message: result.key,
      );
    } catch (e) {
      response = BaseResponse(
        status: false,
        tag: "Upload Error",
        message: e.toString(),
      );
    }
    return response;
  }

  Future recordEvent({String event, Map<String, String> attributes}) async {
    AnalyticsEvent eventInst = AnalyticsEvent(event);

    if (attributes != null)
      attributes.forEach(
          (key, value) => eventInst.properties.addStringProperty(key, value));

    Amplify.Analytics.recordEvent(event: eventInst);
  }

  Future<String> fetchUserCountryAttribute() async {
    try {
      var res = await Amplify.Auth.fetchUserAttributes();
      var country = 'US';
      res.forEach((element) {
        if(element.userAttributeKey == 'custom:country'){
          country = element.value;
        }
        print('key: ${element.userAttributeKey}; value: ${element.value}');
      });
      return country.toUpperCase();
    } on AuthException catch (e) {
      print(e.message);
      return 'No country';
    }
  }

  Future<List<AuthUserAttribute>> fetchUserAttributes() async {
    try {
      List<AuthUserAttribute> _atts = await Amplify.Auth.fetchUserAttributes();
      return _atts;
    } on AuthException catch (e) {
      print(e.message);
      return [];
    }
  }
}
