class FetchTokenResponse{
  bool status;
  String idToken;
  String sub;
  String identityId;
  String message;
  String tag;
  String refreshToken;
  String accessToken;

  FetchTokenResponse({bool status, String idToken, String sub, String identityId, String message, String tag, String refreshToken, String accessToken}) {
    this.status = status;
    this.idToken = idToken;
    this.sub = sub;
    this.identityId = identityId;
    this.message = message;
    this.tag = tag;
    this.refreshToken = refreshToken;
    this.accessToken = accessToken;
  }

  FetchTokenResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    idToken = json['idToken'];
    sub = json['sub'];
    message = json['message'];
    tag = json['tag'];
    identityId = json['identityId'];
    refreshToken = json['refreshToken'];
    accessToken = json['accessToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['idToken'] = this.idToken;
    data['sub'] = this.sub;
    data['message'] = this.message;
    data['tag'] = this.tag;
    data['identityId'] = this.identityId;
    data['refreshToken'] = this.refreshToken;
    data['accessToken'] = this.accessToken;
    return data;
  }
}