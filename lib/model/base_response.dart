class BaseResponse {
  bool status;
  String message;
  String tag;

  BaseResponse({bool status, String message, String tag}) {
    this.status = status;
    this.message = message;
    this.tag = tag;
  }

  BaseResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    tag = json['alert_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['alert_title'] = this.tag;
    return data;
  }
}